using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace DreamDefender
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        Texture2D levelComplete;
        Texture2D levelCompleteWin;
        Texture2D gameOverScreen;
        Texture2D titleScreen;
        Texture2D titleScreen1;
        Texture2D titleScreen2;
        Texture2D winScreen;
        ZombieMap zMap = new ZombieMap();
        PirateMap pMap = new PirateMap();
        RobotMap rMap = new RobotMap();
        AlienMap aMap = new AlienMap();
        Tile zTile = new Tile();
        Tile pTile = new Tile();
        Tile rTile = new Tile();
        Tile aTile = new Tile();
        Tile fortTile = new Tile();
        Tile player = new Tile();
        int position = 350;
        int positionLeft = 300;
        int positionRight = 400;
        int playerIndex = 0;
        KeyboardState presentKey, pastKey;
        Boolean movingLeft = false;
        Boolean movingRight = false;
        Boolean throwing = false;
        Boolean playerDead = false;
        Boolean gameOver = false;
        int playerTimeSinceLastFrame = 0;
        int playerMillisecondsPerFrame = 75;
        int playerTimeSinceDeath = 3000;
        int playerTimeSinceLevelCompleted = 0;
        int playerTimeSinceLevelCompletedWin = 0;
        int playerTimeSinceGameOver = 0;
        int playerTimeSinceGameWin = 0;
        int enemyTimeSinceLastSpawn = 0;
        int enemyMillisecondsUntilSpawn = 2350;
        int enemyMillisecondsPerFrame = 500;
        Tile[] snowballs;
        Tile[] zombies;
        Tile[] pirates;
        Tile[] robots;
        Tile[] aliens;
        Boolean drawTitleScreen = true;
        Boolean drawTitleScreen1, drawTitleScreen2, drawWinScreen = false;
        Boolean drawPirates, drawRobots, drawAliens, levelCompleted, levelCompletedWin, drawZombies = false;
        int snowballIndex = 0;
        int enemyNumber = 0;
        int enemiesLeft = 30;
        int livesLeft = 3;
        Song newTitleTheme, inGameMusic;
        int timeSinceLastSound = 0;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load all of the needed tile sets, screens, in game notification images and the needed sprite font.
            zTile.TileSet = Content.Load<Texture2D>("ZombieLevel");
            pTile.TileSet = Content.Load<Texture2D>("PirateLevel");
            rTile.TileSet = Content.Load<Texture2D>("RobotLevel");
            aTile.TileSet = Content.Load<Texture2D>("AlienLevel");
            fortTile.TileSet = Content.Load<Texture2D>("SnowFort");
            player.TileSet = Content.Load<Texture2D>("Player");
            font = Content.Load<SpriteFont>("SpriteFont1");
            levelComplete = Content.Load<Texture2D>("LevelComplete");
            levelCompleteWin = Content.Load<Texture2D>("LevelCompleteWin");
            gameOverScreen = Content.Load<Texture2D>("GameOver");
            titleScreen = Content.Load<Texture2D>("TitleScreen");
            titleScreen1 = Content.Load<Texture2D>("TitleScreen1");
            titleScreen2 = Content.Load<Texture2D>("TitleScreen2");
            winScreen = Content.Load<Texture2D>("WinScreen");
            newTitleTheme = Content.Load<Song>("NewTitleTheme");
            inGameMusic = Content.Load<Song>("InGameMusic");
            Sound.LoadContent(Content);

            // Load the snowball arry.
            snowballs = new Tile[20];
            for (int i = 0; i < 20; i++)
            {
                snowballs[i] = new Tile();
                snowballs[i].TileSet = Content.Load<Texture2D>("SnowBall");
            }

            // Load the zombie array.
            zombies = new Tile[30];
            for (int i = 0; i < 30; i++)
            {
                zombies[i] = new Tile();
                zombies[i].TileSet = Content.Load<Texture2D>("Zombie");
                zombies[i].enemyX = zombies[i].pickLane();
                zombies[i].health = 3;
            }

            // Load the pirate array.
            pirates = new Tile[30];
            for (int i = 0; i < 30; i++)
            {
                pirates[i] = new Tile();
                pirates[i].TileSet = Content.Load<Texture2D>("Pirate");
                pirates[i].enemyX = pirates[i].pickLane();
                pirates[i].health = 4;
            }

            // Load the robot array.
            robots = new Tile[30];
            for (int i = 0; i < 30; i++)
            {
                robots[i] = new Tile();
                robots[i].TileSet = Content.Load<Texture2D>("Robot");
                robots[i].enemyX = robots[i].pickLane();
                robots[i].health = 4;
            }

            // Load the alien array.
            aliens = new Tile[30];
            for (int i = 0; i < 30; i++)
            {
                aliens[i] = new Tile();
                aliens[i].TileSet = Content.Load<Texture2D>("Alien");
                aliens[i].enemyX = aliens[i].pickLane();
                aliens[i].health = 5;
            }

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            presentKey = Keyboard.GetState();
            // Allows the game to exit using the escape key.
            if (Keyboard.GetState( ).IsKeyDown(Keys.Escape))
                this.Exit();

            // Start the zombie level when the player goes past the "How to Play" screen.
            if (drawTitleScreen2 && presentKey.IsKeyDown(Keys.Enter) && pastKey.IsKeyUp(Keys.Enter))
            {
                MediaPlayer.Stop(); // Stops Title Theme so the in-game music can play
                drawTitleScreen2 = false;
                drawZombies = true;
            }

            // Start the "How to Play" Screen when the player gets past the "Story" screen.
            if (drawTitleScreen1 && presentKey.IsKeyDown(Keys.Enter) && pastKey.IsKeyUp(Keys.Enter))
            {
                drawTitleScreen1 = false;
                drawTitleScreen2 = true;
            }

            // Start the "Story" screen when the player gets past the "Title Screen"
            if (drawTitleScreen && presentKey.IsKeyDown(Keys.Enter) && pastKey.IsKeyUp(Keys.Enter))
            {
                drawTitleScreen = false;
                drawTitleScreen1 = true;
            }

            // Play the Title Theme Music if the following screens are active
            if (drawTitleScreen || drawTitleScreen1 || drawTitleScreen2 || drawWinScreen)
            {
                if (MediaPlayer.State == MediaState.Stopped)
                {
                    MediaPlayer.Stop();
                    MediaPlayer.Play(newTitleTheme);
                    MediaPlayer.IsRepeating = true;
                }
                else
                {
                    MediaPlayer.Resume();
                }
            }

            // Play the in-game music while in game.
            if (drawZombies || drawPirates || drawRobots || drawAliens)
            {
                if (MediaPlayer.State == MediaState.Stopped)
                {
                    MediaPlayer.Play(inGameMusic);
                    MediaPlayer.IsRepeating = true;
                }
                else
                {
                    MediaPlayer.Resume();
                }
            }

            // After 4 seconds of the Game Over screen, go back to "Title Screen" and prep variables for the Zombie Level
            if (gameOver)
            {
                //MediaPlayer.Stop();
                playerTimeSinceGameOver += gameTime.ElapsedGameTime.Milliseconds;
                if (playerTimeSinceGameOver > 4000) //4000 = 4 seconds of drawing the Win Screen
                {
                    MediaPlayer.Stop();
                    drawZombies = drawPirates = drawRobots = drawAliens = false;
                    gameOver = false;
                    drawTitleScreen = true;
                    livesLeft = 3;
                    playerTimeSinceGameOver = 0;
                    enemiesLeft = 30;
                    enemyNumber = 0;
                    enemyTimeSinceLastSpawn = 0;
                    timeSinceLastSound = 0;
                }
            }

            // After 4 seconds of the "Win Screen", go back to the Title Screen and prep variables for the Zombie Level
            if (drawWinScreen)
            {
                playerTimeSinceGameWin += gameTime.ElapsedGameTime.Milliseconds;
                if (playerTimeSinceGameWin > 4000) //4000 = 4 seconds of drawing the Win Screen
                {
                    drawWinScreen = false;
                    drawTitleScreen = true;
                    livesLeft = 3;
                    playerTimeSinceGameOver = 0;
                    enemiesLeft = 30;
                    enemyNumber = 0;
                    enemyTimeSinceLastSpawn = 0;
                }
            }

            // Display the player sprite that designates a death condition and after 4 seconds of that,
            // prep variables to start the level again. Also, reinitialize the enemy arrays.
            if (playerDead)
            {
                playerIndex = 4;
                playerTimeSinceDeath += gameTime.ElapsedGameTime.Milliseconds;
                if (playerTimeSinceDeath > 3000) // 3000 repesents 3 seconds of showing the player as dead
                {
                    playerDead = false;
                    playerIndex = 0;
                    livesLeft--;
                    enemiesLeft = 30;
                    enemyNumber = 0;
                    playerTimeSinceDeath = 0;
                    if (livesLeft == 0)
                    {
                        gameOver = true;
                    }
                    if (drawZombies)
                    {
                        foreach (Tile zombie in zombies)
                        {
                            zombie.enemyY = -100;
                            zombie.activeEnemy = false;
                            zombie.health = 3;
                        } 
                    }
                    if (drawPirates)
                    {
                        foreach (Tile pirate in pirates)
                        {
                            pirate.enemyY = -100;
                            pirate.activeEnemy = false;
                            pirate.health = 4;
                        }
                    }
                    if (drawRobots)
                    {
                        foreach (Tile robot in robots)
                        {
                            robot.enemyY = -100;
                            robot.activeEnemy = false;
                            robot.health = 4;
                        }
                    }
                    if (drawAliens)
                    {
                        foreach (Tile alien in aliens)
                        {
                            alien.enemyY = -100;
                            alien.activeEnemy = false;
                            alien.health = 5;
                        }
                    }
                }
            }

            // When the player completes a level, show the front-facing character sprite and after 5 seconds
            // prep variables for use in the next level (or for the Win Screen). Also, switch to the next level,
            // or the Win Screen if the player is on the last level.
            if (levelCompleted || levelCompletedWin)
            {
                playerIndex = 3;
                playerTimeSinceLevelCompleted += gameTime.ElapsedGameTime.Milliseconds;
                playerTimeSinceLevelCompletedWin += gameTime.ElapsedGameTime.Milliseconds;
                if (playerTimeSinceLevelCompleted > 5000 || playerTimeSinceLevelCompletedWin >5000)
                {
                    levelCompleted = false;
                    levelCompletedWin = false;
                    enemiesLeft = 30;
                    if (levelCompletedWin)
                    {
                        enemiesLeft = 0;
                    }
                    enemyNumber = 0;
                    playerIndex = 0;
                    playerTimeSinceLevelCompleted = 0;
                    playerTimeSinceLevelCompletedWin = 0;
                    if (drawZombies)
                    {
                        drawZombies = false;
                        drawPirates = true;
                    }
                    else if (drawPirates)
                    {
                        drawPirates = false;
                        drawRobots = true;
                    }
                    else if (drawRobots)
                    {
                        drawRobots = false;
                        drawAliens = true;
                    }
                    else if (drawAliens)
                    {
                        drawAliens = false;
                        drawWinScreen = true;
                        MediaPlayer.Stop(); //Stop the in game music
                    }
                }
            }

            // If player and level are active, if the player presses the right arrow, move character to the
            // right adjacent lane.
            if (movingRight && !playerDead && !levelCompleted)
            {
                if (position < positionRight)
                {
                    position += 25;
                }
                else
                {
                    positionRight = position + 50;
                    positionLeft = position - 50;
                    movingRight = false;
                }
            }

            // If player and level are active, if the player presses the left arrow, move character to the
            // left adjacent lane.
            else if (movingLeft && !playerDead && !levelCompleted)
            {
                if (position > positionLeft)
                {
                    position -= 25;
                }
                else
                {
                    positionRight = position + 50;
                    positionLeft = position - 50;
                    movingLeft = false;
                }
            }

            // If player and level are active, if the player presses the space button, animate the character to
            // the throwing sprite frame.
            else if (throwing && !playerDead && !levelCompleted)
            {
                playerTimeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (playerTimeSinceLastFrame < playerMillisecondsPerFrame)
                {
                    playerIndex = 2;
                }
                else
                {
                    playerIndex = 0;
                    throwing = false;
                }
            }

            // Prime the next pass of Update() to move the character right
            else if (presentKey.IsKeyDown(Keys.Right) && pastKey.IsKeyUp(Keys.Right) && positionRight < 700 && !playerDead && !levelCompleted && !levelCompletedWin)
            {
                movingRight = true;
            }

            // Prime the next pass of Update() to move the character left
            else if (presentKey.IsKeyDown(Keys.Left) && pastKey.IsKeyUp(Keys.Left) && positionLeft > 0 && !playerDead && !levelCompleted && !levelCompletedWin)
            {
                movingLeft = true;
            }

            // Prime the next pass of Update() to have the character throw and prime a snowball for movement
            else if (presentKey.IsKeyDown(Keys.Space) && pastKey.IsKeyUp(Keys.Space) && !playerDead && !levelCompleted && !levelCompletedWin)
            {
                throwing = true;
                playerTimeSinceLastFrame = 0;
                if (snowballIndex == 19)
                {
                    snowballIndex = 0;
                }
                else
                {
                    snowballIndex++;
                }
                snowballs[snowballIndex].activeSnowball = true;
                snowballs[snowballIndex].snowballX = position;
                Sound.Throw();
            }

            // Conditions for the position of snowball and wether or not its an active snowball
            for (int i = 0; i < 20; i++ )
            {
                if (snowballs[i].activeSnowball)
                {
                    snowballs[i].snowballY -= 4;
                }

                if (!snowballs[i].activeSnowball)
                {
                    snowballs[i].snowballY = 350;
                }

                if (snowballs[i].snowballY < -52)
                {
                    snowballs[i].activeSnowball = false;
                }
            }

            // Calls the method to draw enemies depending on which Boolean is active...drawZombies, drawPirates, etc.
            if (drawZombies && !playerDead && !levelCompleted && !drawTitleScreen && !drawTitleScreen1 && !drawTitleScreen2)
            {
                DrawZombieHorde(gameTime);
            }

            if (drawPirates && !playerDead && !levelCompleted && !drawTitleScreen && !drawTitleScreen1 && !drawTitleScreen2)
            {
                DrawPirateHorde(gameTime);
            }

            if (drawRobots && !playerDead && !levelCompleted && !drawTitleScreen && !drawTitleScreen1 && !drawTitleScreen2)
            {
                DrawRobotHorde(gameTime);
            }

            if (drawAliens && !playerDead && !levelCompletedWin && !drawTitleScreen && !drawTitleScreen1 && !drawTitleScreen2)
            {
                DrawAlienHorde(gameTime);
            }

            // Used to check button presses.
            pastKey = presentKey;
            base.Update(gameTime);
        } //END UPDATE METHOD

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            // Sets spawn length for zombies, draws the tile map, and updates each enemy's position on screen.
            if (drawZombies)
            {
                enemyMillisecondsUntilSpawn = 2350;
                for (int y = 0; y < 6; y++)
                {
                    for (int x = 0; x < 8; x++)
                    {
                        spriteBatch.Draw(zTile.TileSet,
                            new Rectangle((x * 100), (y * 100), 100, 100),
                            zTile.GetSourceRectangle(zMap.Rows[y].Columns[x].TileID),
                            Color.White);
                    }
                }

                for (int i = 0; i < 30; i++)
                {
                    spriteBatch.Draw(zombies[i].TileSet,
                        new Rectangle(zombies[i].enemyX, zombies[i].enemyY, 100, 100),
                        zombies[i].GetSourceRectangle(zombies[i].enemyFrame), Color.White);
                }
            }

            // Sets spawn length for pirates, draws the tile map, and updates each enemy's position on screen.
            if (drawPirates)
            {
                enemyMillisecondsUntilSpawn = 2350;
                for (int y = 0; y < 6; y++)
                {
                    for (int x = 0; x < 8; x++)
                    {
                        spriteBatch.Draw(pTile.TileSet,
                            new Rectangle((x * 100), (y * 100), 100, 100),
                            pTile.GetSourceRectangle(pMap.Rows[y].Columns[x].TileID),
                            Color.White);
                    }
                }

                for (int i = 0; i < 30; i++)
                {
                    spriteBatch.Draw(pirates[i].TileSet,
                        new Rectangle(pirates[i].enemyX, pirates[i].enemyY, 100, 100),
                        pirates[i].GetSourceRectangle(pirates[i].enemyFrame), Color.White);
                }
            }

            // Sets spawn length for robots, draws the tile map, and updates each enemy's position on screen.
            if (drawRobots)
            {
                enemyMillisecondsUntilSpawn = 1800;
                for (int y = 0; y < 6; y++)
                {
                    for (int x = 0; x < 8; x++)
                    {
                        spriteBatch.Draw(rTile.TileSet,
                            new Rectangle((x * 100), (y * 100), 100, 100),
                            rTile.GetSourceRectangle(rMap.Rows[y].Columns[x].TileID),
                            Color.White);
                    }
                }

                for (int i = 0; i < 30; i++)
                {
                    spriteBatch.Draw(robots[i].TileSet,
                        new Rectangle(robots[i].enemyX, robots[i].enemyY, 100, 100),
                        robots[i].GetSourceRectangle(robots[i].enemyFrame), Color.White);
                }
            }

            // Sets spawn length for aliens, draws the tile map, and updates each enemy's position on screen.
            if (drawAliens)
            {
                enemyMillisecondsUntilSpawn = 1800;
                for (int y = 0; y < 6; y++)
                {
                    for (int x = 0; x < 8; x++)
                    {
                        spriteBatch.Draw(aTile.TileSet,
                            new Rectangle((x * 100), (y * 100), 100, 100),
                            aTile.GetSourceRectangle(aMap.Rows[y].Columns[x].TileID),
                            Color.White);
                    }
                }

                for (int i = 0; i < 30; i++)
                {
                    spriteBatch.Draw(aliens[i].TileSet,
                        new Rectangle(aliens[i].enemyX, aliens[i].enemyY, 100, 100),
                        aliens[i].GetSourceRectangle(aliens[i].enemyFrame), Color.White);
                }
            }

            // Draws the snow fort tiles.
            if (!gameOver && !drawTitleScreen && !drawTitleScreen1 && !drawTitleScreen2)
            {
                for (int x = 0; x < 8; x++)
                {
                    spriteBatch.Draw(fortTile.TileSet,
                        new Rectangle((x * 100), 400, 100, 100),
                        fortTile.GetSourceRectangle(0),
                        Color.White);
                } 
            }

            // Draws each snowball.
            for(int i = 0; i < 20; i++)
            {
                if (snowballs[i].activeSnowball)
                {
                    spriteBatch.Draw(snowballs[i].TileSet, 
                        new Rectangle(snowballs[i].snowballX, snowballs[i].snowballY, 100, 100),
                        snowballs[i].GetSourceRectangle(0), Color.White);
                }
            }

            // Draws the player sprite if any level is active.
            if (drawZombies || drawPirates || drawRobots || drawAliens)
            {
                spriteBatch.Draw(player.TileSet, new Rectangle(position, 400, 100, 100),
                        player.GetSourceRectangle(playerIndex), Color.White); 
            }

            // Draws the Level Completed notification when the player kills 30 enemies.
            if (levelCompleted)
            {
                spriteBatch.Draw(levelComplete, new Vector2(((Window.ClientBounds.X / 2)-50), ((Window.ClientBounds.Y / 2))),
                    null, Color.White, 0.0f, new Vector2(0, 0), 2.5f, SpriteEffects.None, 0.0f);
            }

            // Draws the last Level Completed notification just before the Win Screen.
            if (levelCompletedWin)
            {
                spriteBatch.Draw(levelCompleteWin, new Vector2(((Window.ClientBounds.X / 2) - 50), ((Window.ClientBounds.Y / 2))),
                    null, Color.White, 0.0f, new Vector2(0, 0), 2.5f, SpriteEffects.None, 0.0f);
            }

            // Draws the Game Over screen.
            if (gameOver)
            {
                spriteBatch.Draw(gameOverScreen, Vector2.Zero, Color.White);
            }

            // Draws the "Lives Left" and "Enemies Left" indicators when any level is active.
            if (!gameOver && !drawTitleScreen && !drawTitleScreen1 && !drawTitleScreen2)
            {
                String output = "Enemies Left: " + enemiesLeft;
                String lives = "Lives: " + livesLeft;
                spriteBatch.DrawString(font, output, new Vector2(5, 5), Color.White);
                spriteBatch.DrawString(font, lives, new Vector2(725, 5), Color.White); 
            }

            // Draws the Title Screens and the Win Screen.
            if (drawTitleScreen)
            {
                spriteBatch.Draw(titleScreen, Vector2.Zero, Color.White);
            }
            if (drawTitleScreen1)
            {
                spriteBatch.Draw(titleScreen1, Vector2.Zero, Color.White);
            }
            if (drawTitleScreen2)
            {
                spriteBatch.Draw(titleScreen2, Vector2.Zero, Color.White);
            }
            if (drawWinScreen)
            {
                spriteBatch.Draw(winScreen, Vector2.Zero, Color.White);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        } // END DRAW METHOD


        protected void DrawZombieHorde(GameTime gameTime)
        {
            //Handles the enemy spawns. Makes the current enemy array element active.
            enemyTimeSinceLastSpawn += gameTime.ElapsedGameTime.Milliseconds;
            if (enemyTimeSinceLastSpawn > enemyMillisecondsUntilSpawn)
            {
                enemyTimeSinceLastSpawn = 0;
                if (enemyNumber == 30)
                {
                    enemyNumber = 0;
                }
                if (!levelCompleted)
                {
                    zombies[enemyNumber].activeEnemy = true;
                    enemyNumber++;
                }
            }

            //Handles the enemy walking animation and also the animation when the enemy is hit by a snowball.
            for (int i = 0; i < 30; i++)
            {
                zombies[i].timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (zombies[i].activeEnemy && !zombies[i].enemyDead && !zombies[i].enemyHit)
                {
                    if (!playerDead)
                    {
                        zombies[i].enemyY += 1; 
                    }
                    if (zombies[i].timeSinceLastFrame > enemyMillisecondsPerFrame)
                    {
                        zombies[i].timeSinceLastFrame = 0;
                        zombies[i].enemyFrame++;
                        if (zombies[i].enemyFrame > 1)
                        {
                            zombies[i].enemyFrame = 0;
                        }
                    }
                }
                if (zombies[i].activeEnemy && !zombies[i].enemyDead && zombies[i].enemyHit)
                {
                    zombies[i].timeSinceHit += gameTime.ElapsedGameTime.Milliseconds;
                    zombies[i].enemyY += 1;
                    zombies[i].enemyFrame += 2;
                    if (zombies[i].enemyFrame > 3)
                    {
                        zombies[i].enemyFrame -= 2;
                    }
                    if (zombies[i].timeSinceHit > 50) //50 milliseconds is the time of hit animation
                    {
                        zombies[i].enemyFrame = 0;
                        zombies[i].enemyHit = false;
                        zombies[i].timeSinceHit = 0;
                    }
                    
                }
            }

            // Collision detection for the zombies. Checks each enemy against each snowball for a collision.
            foreach (Tile snowball in snowballs)
            {
                foreach (Tile zombie in zombies)
                {
                    if (zombie.EnemyGetHitBox().Intersects(snowball.SnowballGetHitBox()) && snowball.activeSnowball)
                    {
                        zombie.enemyHit = true;
                        snowball.activeSnowball = false;
                        zombie.health--;
                        Sound.HitZombie();
                        if (zombie.health == 0)
                        {
                            zombie.activeEnemy = false;
                            zombie.enemyY = -100;
                            enemiesLeft--;
                        }
                    }
                }
            }

            // Sets the "Level Completed" condition when there are no enemies left
            if (enemiesLeft == 0 && !levelCompleted)
            {
                levelCompleted = true;
                playerTimeSinceLevelCompleted = 0;
            }

            // In the event of a game over, reset all the enemy properties to default
            if (gameOver || levelCompleted)
            {
                foreach (Tile zombie in zombies)
                {
                    zombie.enemyY = -100;
                    zombie.activeEnemy = false;
                    zombie.health = 3;
                }
            }

            // Sets the "Player Dead" condition when an enemy reaches the snow fort
            if (!playerDead)
            {
                foreach (Tile zombie in zombies)
                {
                    if (zombie.enemyY == 400)
                    {
                        playerDead = true;
                        playerIndex = 4;
                        playerTimeSinceDeath = 0;
                    }
                }
            }

            // Plays a background sound effect every 2 seconds (2000 milliseconds)
            timeSinceLastSound += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastSound > 3500 && !gameOver)
            {
                Sound.BGZombie();
                timeSinceLastSound = 0;
            }
        }

        // Identical to the DrawZombieHorde method, but for Pirates
        protected void DrawPirateHorde(GameTime gameTime)
        {
            enemyTimeSinceLastSpawn += gameTime.ElapsedGameTime.Milliseconds;
            if (enemyTimeSinceLastSpawn > enemyMillisecondsUntilSpawn)
            {
                enemyTimeSinceLastSpawn = 0;
                if (enemyNumber == 30)
                {
                    enemyNumber = 0;
                }
                if (!levelCompleted)
                {
                    pirates[enemyNumber].activeEnemy = true;
                    enemyNumber++;
                }
            }

            for (int i = 0; i < 30; i++)
            {
                pirates[i].timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (pirates[i].activeEnemy && !pirates[i].enemyDead && !pirates[i].enemyHit)
                {
                    if (!playerDead)
                    {
                        pirates[i].enemyY += 1;
                    }
                    if (pirates[i].timeSinceLastFrame > enemyMillisecondsPerFrame)
                    {
                        pirates[i].timeSinceLastFrame = 0;
                        pirates[i].enemyFrame++;
                        if (pirates[i].enemyFrame > 1)
                        {
                            pirates[i].enemyFrame = 0;
                        }
                    }
                }
                if (pirates[i].activeEnemy && !pirates[i].enemyDead && pirates[i].enemyHit)
                {
                    pirates[i].timeSinceHit += gameTime.ElapsedGameTime.Milliseconds;
                    pirates[i].enemyY += 1;
                    pirates[i].enemyFrame += 2;
                    if (pirates[i].enemyFrame > 3)
                    {
                        pirates[i].enemyFrame -= 2;
                    }
                    if (pirates[i].timeSinceHit > 50)
                    {
                        pirates[i].enemyFrame = 0;
                        pirates[i].enemyHit = false;
                        pirates[i].timeSinceHit = 0;
                    }

                }
            }
            foreach (Tile snowball in snowballs)
            {
                foreach (Tile pirate in pirates)
                {
                    if (pirate.EnemyGetHitBox().Intersects(snowball.SnowballGetHitBox()) && snowball.activeSnowball)
                    {
                        pirate.enemyHit = true;
                        snowball.activeSnowball = false;
                        pirate.health--;
                        Sound.HitPirate();
                        if (pirate.health == 0)
                        {
                            pirate.activeEnemy = false;
                            pirate.enemyY = -100;
                            enemiesLeft--;
                        }
                    }
                }
            }

            if (enemiesLeft == 0 && !levelCompleted)
            {
                levelCompleted = true;
                playerTimeSinceLevelCompleted = 0;
            }

            if (gameOver || levelCompleted)
            {
                foreach (Tile pirate in pirates)
                {
                    pirate.enemyY = -100;
                    pirate.activeEnemy = false;
                    pirate.health = 4;
                }
            }

            if (!playerDead)
            {
                foreach (Tile pirate in pirates)
                {
                    if (pirate.enemyY == 400)
                    {
                        playerDead = true;
                        playerIndex = 4;
                        playerTimeSinceDeath = 0;
                    }
                }
            }

            timeSinceLastSound += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastSound > 4500 && !gameOver)
            {
                Sound.BGPirate();
                timeSinceLastSound = 0;
            }
        }

        // Identical to the DrawZombieHorde method, but for Robots
        protected void DrawRobotHorde(GameTime gameTime)
        {
            enemyTimeSinceLastSpawn += gameTime.ElapsedGameTime.Milliseconds;
            if (enemyTimeSinceLastSpawn > enemyMillisecondsUntilSpawn)
            {
                enemyTimeSinceLastSpawn = 0;
                if (enemyNumber == 30)
                {
                    enemyNumber = 0;
                }
                if (!levelCompleted)
                {
                    robots[enemyNumber].activeEnemy = true;
                    enemyNumber++;
                }
            }

            for (int i = 0; i < 30; i++)
            {
                robots[i].timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (robots[i].activeEnemy && !robots[i].enemyDead && !robots[i].enemyHit)
                {
                    if (!playerDead)
                    {
                        robots[i].enemyY += 1;
                    }
                    if (robots[i].timeSinceLastFrame > enemyMillisecondsPerFrame)
                    {
                        robots[i].timeSinceLastFrame = 0;
                        robots[i].enemyFrame++;
                        if (robots[i].enemyFrame > 1)
                        {
                            robots[i].enemyFrame = 0;
                        }
                    }
                }
                if (robots[i].activeEnemy && !robots[i].enemyDead && robots[i].enemyHit)
                {
                    robots[i].timeSinceHit += gameTime.ElapsedGameTime.Milliseconds;
                    robots[i].enemyY += 1;
                    robots[i].enemyFrame += 2;
                    if (robots[i].enemyFrame > 3)
                    {
                        robots[i].enemyFrame -= 2;
                    }
                    if (robots[i].timeSinceHit > 50)
                    {
                        robots[i].enemyFrame = 0;
                        robots[i].enemyHit = false;
                        robots[i].timeSinceHit = 0;
                    }

                }
            }
            foreach (Tile snowball in snowballs)
            {
                foreach (Tile robot in robots)
                {
                    if (robot.EnemyGetHitBox().Intersects(snowball.SnowballGetHitBox()) && snowball.activeSnowball)
                    {
                        robot.enemyHit = true;
                        snowball.activeSnowball = false;
                        robot.health--;
                        Sound.HitRobot();
                        if (robot.health == 0)
                        {
                            robot.activeEnemy = false;
                            robot.enemyY = -100;
                            enemiesLeft--;
                        }
                    }
                }
            }

            if (enemiesLeft == 0 && !levelCompleted)
            {
                levelCompleted = true;
                playerTimeSinceLevelCompleted = 0;
            }

            if (gameOver || levelCompleted)
            {
                foreach (Tile robot in robots)
                {
                    robot.enemyY = -100;
                    robot.activeEnemy = false;
                    robot.health = 4;
                }
            }

            if (!playerDead)
            {
                foreach (Tile robot in robots)
                {
                    if (robot.enemyY == 400)
                    {
                        playerDead = true;
                        playerIndex = 4;
                        playerTimeSinceDeath = 0;
                    }
                }
            }

            timeSinceLastSound += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastSound > 2000 && !gameOver)
            {
                Sound.BGRobot();
                timeSinceLastSound = 0;
            }
        }

        // Identical to the DrawZombieHorde method, but for Aliens
        protected void DrawAlienHorde(GameTime gameTime)
        {
            enemyTimeSinceLastSpawn += gameTime.ElapsedGameTime.Milliseconds;
            if (enemyTimeSinceLastSpawn > enemyMillisecondsUntilSpawn)
            {
                enemyTimeSinceLastSpawn = 0;
                if (enemyNumber == 30)
                {
                    enemyNumber = 0;
                }
                if (!levelCompletedWin)
                {
                    aliens[enemyNumber].activeEnemy = true;
                    enemyNumber++;
                }
            }

            for (int i = 0; i < 30; i++)
            {
                aliens[i].timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (aliens[i].activeEnemy && !aliens[i].enemyDead && !aliens[i].enemyHit && !levelCompletedWin)
                {
                    if (!playerDead)
                    {
                        aliens[i].enemyY += 1;
                    }
                    if (aliens[i].timeSinceLastFrame > enemyMillisecondsPerFrame)
                    {
                        aliens[i].timeSinceLastFrame = 0;
                        aliens[i].enemyFrame++;
                        if (aliens[i].enemyFrame > 1)
                        {
                            aliens[i].enemyFrame = 0;
                        }
                    }
                }
                if (aliens[i].activeEnemy && !aliens[i].enemyDead && aliens[i].enemyHit)
                {
                    aliens[i].timeSinceHit += gameTime.ElapsedGameTime.Milliseconds;
                    aliens[i].enemyY += 1;
                    aliens[i].enemyFrame += 2;
                    if (aliens[i].enemyFrame > 3)
                    {
                        aliens[i].enemyFrame -= 2;
                    }
                    if (aliens[i].timeSinceHit > 50)
                    {
                        aliens[i].enemyFrame = 0;
                        aliens[i].enemyHit = false;
                        aliens[i].timeSinceHit = 0;
                    }

                }
            }
            foreach (Tile snowball in snowballs)
            {
                foreach (Tile alien in aliens)
                {
                    if (alien.EnemyGetHitBox().Intersects(snowball.SnowballGetHitBox()) && snowball.activeSnowball)
                    {
                        alien.enemyHit = true;
                        snowball.activeSnowball = false;
                        alien.health--;
                        Sound.HitAlien();
                        if (alien.health == 0)
                        {
                            alien.activeEnemy = false;
                            alien.enemyY = -100;
                            enemiesLeft--;
                        }
                    }
                }
            }

            if (enemiesLeft == 0 && !levelCompletedWin)
            {
                levelCompletedWin = true;
                playerTimeSinceLevelCompletedWin = 0;
            }

            if (gameOver || levelCompletedWin)
            {
                foreach (Tile alien in aliens)
                {
                    alien.enemyY = -100;
                    alien.activeEnemy = false;
                    alien.health = 5;
                }
            }

            if (!playerDead)
            {
                foreach (Tile alien in aliens)
                {
                    if (alien.enemyY == 400)
                    {
                        playerDead = true;
                        playerIndex = 4;
                        playerTimeSinceDeath = 0;
                    }
                }
            }

            timeSinceLastSound += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastSound > 2000 && !gameOver)
            {
                Sound.BGAlien();
                timeSinceLastSound = 0;
            }
        }
    }
}
