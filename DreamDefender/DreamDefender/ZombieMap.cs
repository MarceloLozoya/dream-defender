﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DreamDefender
{
    class ZombieMap
    {
        public List<ZMapRow> Rows = new List<ZMapRow>();

        public ZombieMap()
        {
            // 6 tile rows for the map
            for (int y = 0; y < 6; y++)
            {
                ZMapRow row = new ZMapRow();

                // 8 tile columns for the map
                for (int x = 0; x < 8; x++)
                {
                    row.Columns.Add(new MapCell(0));
                }
                Rows.Add(row);
            }

            // Set the tile ID's to the correct tileset tiles
            Rows[0].Columns[0].TileID = 0;
            Rows[0].Columns[1].TileID = 2;
            Rows[0].Columns[2].TileID = 2;
            Rows[0].Columns[3].TileID = 2;
            Rows[0].Columns[4].TileID = 2;
            Rows[0].Columns[5].TileID = 2;
            Rows[0].Columns[6].TileID = 2;
            Rows[0].Columns[7].TileID = 3;

            Rows[1].Columns[0].TileID = 0;
            Rows[1].Columns[1].TileID = 1;
            Rows[1].Columns[2].TileID = 1;
            Rows[1].Columns[3].TileID = 1;
            Rows[1].Columns[4].TileID = 1;
            Rows[1].Columns[5].TileID = 1;
            Rows[1].Columns[6].TileID = 1;
            Rows[1].Columns[7].TileID = 3;

            Rows[2].Columns[0].TileID = 0;
            Rows[2].Columns[1].TileID = 1;
            Rows[2].Columns[2].TileID = 1;
            Rows[2].Columns[3].TileID = 1;
            Rows[2].Columns[4].TileID = 1;
            Rows[2].Columns[5].TileID = 1;
            Rows[2].Columns[6].TileID = 1;
            Rows[2].Columns[7].TileID = 3;

            Rows[3].Columns[0].TileID = 0;
            Rows[3].Columns[1].TileID = 1;
            Rows[3].Columns[2].TileID = 1;
            Rows[3].Columns[3].TileID = 1;
            Rows[3].Columns[4].TileID = 1;
            Rows[3].Columns[5].TileID = 1;
            Rows[3].Columns[6].TileID = 1;
            Rows[3].Columns[7].TileID = 3;

            Rows[4].Columns[0].TileID = 0;
            Rows[4].Columns[1].TileID = 1;
            Rows[4].Columns[2].TileID = 1;
            Rows[4].Columns[3].TileID = 1;
            Rows[4].Columns[4].TileID = 1;
            Rows[4].Columns[5].TileID = 1;
            Rows[4].Columns[6].TileID = 1;
            Rows[4].Columns[7].TileID = 3;

            Rows[5].Columns[0].TileID = 0;
            Rows[5].Columns[1].TileID = 1;
            Rows[5].Columns[2].TileID = 1;
            Rows[5].Columns[3].TileID = 1;
            Rows[5].Columns[4].TileID = 1;
            Rows[5].Columns[5].TileID = 1;
            Rows[5].Columns[6].TileID = 1;
            Rows[5].Columns[7].TileID = 3;
            
        }
        
    }

    class ZMapRow
    {
        public List<MapCell> Columns = new List<MapCell>();
    }

}
