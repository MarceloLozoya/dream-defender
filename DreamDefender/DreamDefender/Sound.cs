﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace DreamDefender
{
    static class Sound
    {
        static SoundEffect alien1, alien2, alien3, alien4, alien5;
        static SoundEffect pirate1, pirate2, pirate3, pirate4, pirate5;
        static SoundEffect robot1, robot2, robot3, robot4, robot5;
        static SoundEffect zombie1, zombie2, zombie3, zombie4, zombie5;
        static SoundEffectInstance zombie5Instance, robot5Instance, pirate4Instance, alien4Instance;
        static SoundEffect throwSound;
        static Random random = new Random();
        static int effectNumber;

        static public void LoadContent(ContentManager Content)
        {
            alien1 = Content.Load<SoundEffect>("Alien1");
            alien2 = Content.Load<SoundEffect>("Alien2");
            alien3 = Content.Load<SoundEffect>("Alien3");
            alien4 = Content.Load<SoundEffect>("Alien4");
            alien4Instance = alien4.CreateInstance();
            alien5 = Content.Load<SoundEffect>("Alien5");
            pirate1 = Content.Load<SoundEffect>("Pirates1");
            pirate2 = Content.Load<SoundEffect>("Pirates2");
            pirate3 = Content.Load<SoundEffect>("Pirates3");
            pirate4 = Content.Load<SoundEffect>("Pirates4");
            pirate4Instance = pirate4.CreateInstance();
            pirate5 = Content.Load<SoundEffect>("Pirates5");
            robot1 = Content.Load<SoundEffect>("Robot1");
            robot2 = Content.Load<SoundEffect>("Robot2");
            robot3 = Content.Load<SoundEffect>("Robot3");
            robot4 = Content.Load<SoundEffect>("Robot4");
            robot5 = Content.Load<SoundEffect>("Robot5");
            robot5Instance = robot5.CreateInstance();
            zombie1 = Content.Load<SoundEffect>("Zombies1");
            zombie2 = Content.Load<SoundEffect>("Zombies2");
            zombie3 = Content.Load<SoundEffect>("Zombies3");
            zombie4 = Content.Load<SoundEffect>("Zombies4");
            zombie5 = Content.Load<SoundEffect>("Zombies5");
            zombie5Instance = zombie5.CreateInstance();
            throwSound = Content.Load<SoundEffect>("Throw");
        }

        // Plays the throw sound when the player throws a snowball
        static public void Throw()
        {
            throwSound.Play();
        }

        // Plays a sound when a zombie gets hit
        static public void HitZombie()
        {
            if (zombie5Instance.State == SoundState.Stopped)
            {
                zombie5Instance.Play();
            }
            else
            {
                zombie5Instance.Resume();
            }
        }

        // Plays a sound when a pirate gets hit
        static public void HitPirate()
        {
            if (pirate4Instance.State == SoundState.Stopped)
            {
                pirate4Instance.Play();
            }
            else
            {
                pirate4Instance.Resume();
            }
        }

        // Plays a sound when a robot gets hit
        static public void HitRobot()
        {
            if (robot5Instance.State == SoundState.Stopped)
            {
                robot5Instance.Play();
            }
            else
            {
                robot5Instance.Resume();
            }
        }

        // Plays a sound when an alien gets hit
        static public void HitAlien()
        {
            if (alien4Instance.State == SoundState.Stopped)
            {
                alien4Instance.Play();
            }
            else
            {
                alien4Instance.Resume();
            }
        }

        // Plays a random background sound for zombies
        static public void BGZombie()
        {
            effectNumber = random.Next(1, 6);
            if (effectNumber == 1)
            {
                zombie1.Play();
            }
            if (effectNumber == 2)
            {
                zombie2.Play();
            }
            if (effectNumber == 3)
            {
                zombie3.Play();
            }
            if (effectNumber == 4)
            {
                zombie4.Play();
            }
            if (effectNumber == 5)
            {
                //Play no sound
            }
        }

        // Plays a random background sound for pirates
        static public void BGPirate()
        {
            effectNumber = random.Next(1, 6);
            if (effectNumber == 1)
            {
                pirate1.Play();
            }
            if (effectNumber == 2)
            {
                pirate2.Play();
            }
            if (effectNumber == 3)
            {
                pirate3.Play();
            }
            if (effectNumber == 4)
            {
                pirate5.Play();
            }
            if (effectNumber == 5)
            {
                //Play no sound
            }
        }

        // Plays a random background sound for robots
        static public void BGRobot()
        {
            effectNumber = random.Next(1, 6);
            if (effectNumber == 1)
            {
                robot1.Play();
            }
            if (effectNumber == 2)
            {
                robot2.Play();
            }
            if (effectNumber == 3)
            {
                robot3.Play();
            }
            if (effectNumber == 4)
            {
                robot4.Play();
            }
            if (effectNumber == 5)
            {
                //Play no sound
            }
        }

        // Plays a random background sound for aliens
        static public void BGAlien()
        {
            effectNumber = random.Next(1, 6);
            if (effectNumber == 1)
            {
                alien1.Play();
            }
            if (effectNumber == 2)
            {
                alien2.Play();
            }
            if (effectNumber == 3)
            {
                alien3.Play();
            }
            if (effectNumber == 4)
            {
                alien5.Play();
            }
            if (effectNumber == 5)
            {
                //Play no sound
            }
        }
    } // End Sound Class
}
