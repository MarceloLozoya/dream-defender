﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DreamDefender
{
    class MapCell
    {
        public int TileID { get; set; }

        // Used by the four map classes to get and set the tileID
        public MapCell(int tileID)
        {
            TileID = tileID;
        }
    }
}
