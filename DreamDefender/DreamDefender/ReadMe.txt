﻿Game Title: Dream Defender
By: Marcelo Lozoya

How to Play:
Press the left and right arrows to move the character left and right.
Press the space bar to throw snowballs at the enemies.
If an enemy reaches the snow fort, the player dies.
The player gets 3 lives.



All music, sprites, art by Marcelo Lozoya
All robot and alien sound effects by Marcelo Lozoya

Zombie sound effects taken from http://soundbible.com/tags-zombie.html
Pirate sound effects taken from http://homepage.westmont.edu/bstrine/yarrgh/
Throwing sound effect taken from http://soundbible.com/2068-Woosh.html