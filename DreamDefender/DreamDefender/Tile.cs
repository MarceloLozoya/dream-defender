﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DreamDefender
{
    class Tile
    {
        public Texture2D TileSet;
        public Boolean activeSnowball = false;
        public Boolean activeEnemy = false;
        public Boolean enemyDead = false;
        public Boolean enemyHit = false;
        public int snowballY = 700;
        public int snowballX;
        public int enemyY = -100;
        public int enemyX;
        public int enemyFrame = 0;
        public int health;
        public int timeSinceLastFrame = 0;
        public int timeSinceHit = 0;
        private static Random random = new Random();

        // Used to find which tile to retrieve from a tileset
        public Rectangle GetSourceRectangle(int Index)
        {
            return new Rectangle(Index * 100, 0, 100, 100);
        }

        // Used to find the position of the current enemy's hit box
        public Rectangle EnemyGetHitBox()
        {
            return new Rectangle(enemyX + 25, enemyY, 50, 50);
        }

        // Used to find the position of the current snowball's hit box
        public Rectangle SnowballGetHitBox()
        {
            return new Rectangle(snowballX + 25, snowballY+50, 50, 50);
        }

        // Used to pick a random lane for the enemies to walk down
        public int pickLane()
        {
            int lane = random.Next(1, 14);
            int lanePixel = (lane*50);
            return lanePixel;
        }

    }
}
